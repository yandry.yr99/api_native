import { Component } from '@angular/core';
import { Contacts, ContactFieldType, IContactFindOptions, Contact, ContactName, ContactField } from '@ionic-native/contacts';
import { NavController, ToastController } from '@ionic/angular';
import { async } from '@angular/core/testing';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  ourtype: ContactFieldType[] = ["displayName"];
  contactsFound = [];

  constructor(public navCtrl: NavController, private contacts: Contacts, private callNumber: CallNumber, private toastCtrl:ToastController) {
    this.search('');
  }
  
  create(){
    let contact:Contact=this.contacts.create()
    contact.name = new ContactName(null,'Albus', 'Ape')
    contact.phoneNumbers = [new ContactField('mobile','0123456789')];
    contact.save().then(
      async()=>{
        let toast = await this.toastCtrl.create({
          message:'Contact add!'
        });
        toast.present();
      },
      (error:any) => console.error('Error guardar contacto.', error)
    );
     
  }

  call(contact:Contact){
    this.callNumber.callNumber(contact.phoneNumbers[0].value, true);
  }

  search(q){
    const option: IContactFindOptions= {
      filter: q
    }
    this.contacts.find(this.ourtype, option).then(conts =>{
      this.contactsFound = conts;
    })

  }

  onKeyUp(ev){
    this.search(ev.target.value);
  }

}
