import { Component, OnInit, Input } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Contacts, ContactFieldType, IContactFindOptions, Contact, ContactName, ContactField } from '@ionic-native/contacts';

@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.scss'],
})
export class CallComponent implements OnInit {

  @Input() numero:string;

  constructor(private callNumber: CallNumber) { }

  ngOnInit() {}

  call(){
    this.callNumber.callNumber(this.numero, true)
    //console.log(this.numero)
  }

  

}
