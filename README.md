Utilizamos las api de contacs y call para nuestra práctica
- Instalación de la Api de contacs
**
1. npm install cordova-plugin-contacts

2. npm install @ionic-native/contacts
3. ionic cap sync**

- Instalación de la Api de call
**
1. npm install call-number

2. npm install @ionic-native/call-number
3. ionic cap sync**

- Luego nos dirigimos a app.module.ts y vamos agregar las api a nuestro proyecto
![Screenshot](1.PNG)

- Luegos nos dirigimos al home.page.ts donde vamos a colocar en el constructor private contacts: Contacts que nos sirve para podr utilizar esta api y luego creamos los métodos para buscar los contactos y para recibir la información del celular.
![Screenshot](2.PNG)
![Screenshot](3.PNG)
- En el html de home vamos a mostrar el método buscar y luego vamos a mostrar los contactos y el número
![Screenshot](4.PNG)
- Luego creamos nuestro componente para realizar las llamadas y en el componente.ts creamos lo siguiente
![Screenshot](5.PNG)
- Posteriormente creamos el html de nuestro componente y lo llamamos en el html home así: <app-call [numero]="contact.phoneNumbers[0].value"></app-call>
![Screenshot](6.PNG)
- Resultado Final
![Screenshot](contacts.PNG)
![Screenshot](call.PNG)



